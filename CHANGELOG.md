# Changelog

This project follows the [Semantic Versioning 2.0.0](https://semver.org/) convention.

## v3.1.2

* Fixes #64 by adding `auto_stop_in` properties to periodically cleanup old buckets and change sets
* Fixes #66 by adding `cleanup_bucket` after `execute_change_set`

## v3.1.1

* Added `needs: []` to cfn_nag job to speed up execution
* Changed cfn_nag behavior to treat all findings as blocking and encourage best practices to address or suppress findings instead of ignoring

## v3.1.0

* Added `resouce_group`s to prevent collisions in CloudFormation jobs among and between pipelines

## v3.0.8

* Resolve #60 by exposing cfn-nag results as an artifact
* Changes exit behavior for cfn-nag. cfn-nag will now always pass regardless of findings. Findings can be viewed in the artifacts or job console output.

## v3.0.7

* Fix bug with recursive includes in CI templates

## v3.0.6

* Resolves #61 by exposing resource changes as artifacts

## v3.0.5

* Resolves #57 by supplying a meaningful default for stack set name

## v3.0.4

* Fix bug with extraneous `dev` jobs running on master branch refs

## v3.0.3

* Fix bug for missing `include` for [.gitlab-ci-tag-defs.yml](templates/.gitlab-ci-tag-defs.yml)
* Fix bug by formatting the prefix in a way expected by the `aws cloudformation package` command
* Fix bug by formatting the prefix in a way expected by the `aws cloudformation estimate-template-cost` command
* Fix bug by restoring the `stage_templates_s3` script which is still used by the stack set template

## v3.0.2

* resolve circular dependencies when running local pipelines that the build image must be present first, which previously required a manual workaround

## v3.0.1

* resolve issue with too many leading `/'s` in prefix construction
* resolve stack set template issue with missing yaml anchor
* change default stack name to use the project path slug
* fix bug which left orphan S3 buckets from pipelines run on merge requests
* fix bug which left orphan CloudFormation change sets from pipelines run on merge requests
* fixes stack set deploy issue by uploading templates to S3 before a stack set operation
* removes REGION env var by using the default AWS_DEFAULT_REGION environment variable; no functional changes, just a reduction in code length
* adds missing on_stop actions for deliver jobs for stack sets
* Add light version of CFN CI template

## v3.0.0

* consolidate packaging to the integration test job and deprecate the package job in the build stage
* deprecate `validate-templates`; all requirements met by cfn-lint
* deprecate `cleanup_failed_change_sets` and use unique change set names per commit per pipeline
* make the pipelines more atomic by reusing the same change set in the deploy phase
* add stop actions for cleaning up unexecuted change sets and temporary s3 buckets
* treat all S3 buckets for build artifacts as ephemeral

## v2.6.0

* implement [`aws cloudformation package`](https://docs.aws.amazon.com/cli/latest/reference/cloudformation/package.html) which handles uploads of local files and resolution of local paths
  * `TemplateURL` values for nested stacks must be absolute paths e.g. `/builds/my-group/my-project/cloudformation/template.yml`
  * `Code` for Lambda functions must be absolute paths. It is recommended that you build and package your functions in the `build` stage so you can simply reference them in CloudFormation like `Code: //builds/my-group/my-project/lambda/my_function.zip`. `cfn-python-lint` will throw a warning and you can ignore with:

  ```yml
  Metadata:
    cfn-lint:
      config:
        ignore_checks:
          - W3002 # Using package command, so this warning is not applicable
  ```

* deprecate package_lambda_simple so users can decide the best way to build and package their own functions

## v2.5.14

* implement rules for cfn-lint and cfn_nag jobs to suppress when not needed
* implement rules for yamllint and jsonlint jobs to suppress when not needed

## v2.5.13

* added `ENV` variables to control deployment environments
* added `DELIVER` and `DEPLOY` variables to control deliver and deploy stage execution behavior
* abstracted variables to a separate file for easier reference

## v2.5.12

* added CloudFormation stack set [support](templates/README.md#.gitlab-ci-aws-cfn-stackset.yml)

## v2.5.11

* migrate `delete_change_set` from bash to python

## v2.5.10

* add command line flags for lint-templates

## v2.5.9

* make sure the cfn-lint job always executes with the latest specs

## v2.5.8

* change default region to `us-east-1`
* make bucket names on multiple pipelines of the same commit unique with `$CI_PIPELINE_IID`

## v2.5.7

* run `staging` jobs in merge requests to evaluate code against production
  * allow `dev` pipeline on the branch to run uninterrupted
  * capture all necessary jobs to ensure quality and validity of merge requests
* various CI DRYing
* retirement of `create_describe_only_change_set` "combo" script in favor of two separate scripts
* promote `cfn-lint` to unit testing stage
* reduce complexity in selecting S3 bucket appropriate for the environment

## v2.5.6

* update path for lambda delivery

## v2.5.5

* separate staging and pre-prod S3 buckets to avoid collisions

## v2.5.4

* consolidate lambda deliver jobs
* remove unnecessary anchor from lambda
* fix lambda dev delivery bucket

## v2.5.3

* switch CFN template to use the `stable` image
* reflect bucket naming change with commit SHA in CFN template

## v2.5.2

* fixed #36 and specified instructions for template inclusion

## v2.5.1

* use commit SHA to make temporary bucket names unique across simultaneous pipelines; avoid collisions

## v2.5.0

* adds staging jobs for testing feature branches against prod
* resolves throttling in most ListStacks actions

## v2.4.5

* adds dev versions of AWS jobs to allow running in feature branches without a `prod` tagged runner

## v2.4.0

* updates to Dockerfile to ensure pip dependencies stay updated
* addition of Git CI tests

## v2.3.2

* use the temp bucket for the describe only change set so that it tests against the incoming template changes instead of the current template

## v2.3.1

* fix Lambda CI template stages to align with CFN CI template

## v2.3.0

* add drift detection as a build step prior to creation of a change set for CloudFormation stacks

## v2.2.0

* add support for cfn-python-lint

## v2.1.2

* allow estimate-template-cost to fail because of known issue with nested stacks that pass parameters
* show estimate-template-cost output in the screen

## v2.1.1

* replace all S3 URLs to use virtual host style addressing for regional parity

## v2.1.0

* add estimate_template_cost command

## v2.0.2

* resolve #25 by fixing support for `validate-templates` in us-east-1

## v2.0.1

* run cleanup jobs prior to delivery to avoid being blocked in [templates/.gitlab-ci-aws-cfn.yml](templates/.gitlab-ci-aws-cfn.yml)

## v2.0

* migrate cfn-build, cfn-test, cfn-deploy from their original separate projects to this project
* update [templates/.gitlab-ci-aws-cfn.yml](templates/.gitlab-ci-aws-cfn.yml) with new test stage definitions
* update README files for all modules

## v1.0

* initial version
