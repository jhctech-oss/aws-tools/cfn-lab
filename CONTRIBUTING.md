# Contribution Guidelines

Thank you for contributing! Pooling together knowledge, experience, and resources helps us all develop better/faster/cheaper in the cloud. To ensure quality, the following recommendations are provided:

## CI/CD Tenants

These are our guiding principles:

1. [CloudFormation-only Resource Management](#CloudFormation)
2. [Continuous feedback](#feedback)
3. [Proscribed Best Practices](#best-practices)
4. [Automation wherever possible](#automation)
5. [Manual steps as first-class citizens](#manual)
6. [Keep Your Scripts Platform-Agnostic](#agnostic)
7. [DRY Configurations](#dry)

### 1. CloudFormation-only Resource Management{: name="CloudFormation"}

This is a project designed to make it easier to test and deploy using CloudFormation. As such, whenever possible, no AWS resources used to support CI/CD pipelines should be created or modified outside of CloudFormation. Here's a short list of why management through CloudFormation is better than other methods:

* Reusable

CloudFormation templates are designed to be reused. Common templates should be shared across applications with configuration customization performed using parameters. This reduces the amount of code required to manage AWS environments and makes implementing best practices easier.

* Version-controlled

CloudFormation templates can be version-controlled so there is a record of the changes, who made them, why they were made, and an ability to revert back to an earlier state.

* Change management

CloudFormation keeps records of the resources it changes and why they were changed. Specific ResourceStatusReasons are given as stack events, and change sets specify the update policy and reason for modification or replacement of resources.

* AWS resource dependency-aware

CloudFormation knows which resources need to be built before dependent resources can be started. CloudFormation knows when updates to one resource affect the configuration of another, and intelligently orders those configuration changes to produce the most successful outcome.

* Support for rollbacks

CloudFormation has native rollback capabilities. Rollback conditions can be defined within the template, so an unsuccessful deployment is quickly and efficiently rolled back to the last stable version for maximum application uptime.

### 2. Continuous Feedback{: name="feedback"}

A cornerstone of DevOps is the feedback loop. It is important to get feedback as early as possible to know whether your new release passes tests and what impacts it will have that need to be considered during deployment. To get that feedback, we design modularized test cases. These test cases allow us to conduct a specific check in a controlled environment and isolate it so that all associated output can be captured as part of our feedback loop.

These projects exercise modularity in the following ways:

* Code designed around a unique step in the CI/CD pipeline is separated into a dedicated project. Each project should be designed to perform a job or a category of jobs using code managed solely from that project or inherited from an upstream template.
* Individual scripts employ the [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy); they do exactly one thing very well. Modularizing scripts allows them to be tested once and reused many times.
* Design each script and each project to be minimalist. Keeping the code lean and mean helps ship and run it faster. Consider the business value vs. dependency tradeoffs when introducing or importing a new feature, library, or framework.

### 3. Proscribed Best Practices{: name="best-practices"}

There are lots of ways to accomplish a single task. Pick the best from the bunch and stick with it.

Example: I can update a stack using `update-stack` directly, or I can create a change set. Change sets give me the opportunity to see changes before they happen, so I'm going to write scripts using change sets. Do I have the ability to code support for `update-stack`? Absolutely. Will I? So long as change sets are better, no way.

### 4. Automation wherever possible{: name="automation"}

Automation standardizes processes and reduces the chance of human error. Automation can be reproduced by anyone at anytime, and is less prone to human error. Automation self-documents steps taken to build, test, and deploy an application. Automation helps achieve more reliable and more frequent code deployments by decreasing the time to build and test new code as well as improving overall release quality.

If you can automate it, do it.

### 5. Manual steps as first-class citizens{: name="manual"}

Not all steps can or should be automated. Some steps will need to be performed manually. However, we want those steps to enhance our processes rather than hinder it. Our pipeline supports manual steps as well as it does for automated steps. Nevertheless, we should always evaluate manual steps against technical and business capabilities to offload repeatable steps to automation where possible.

Encourage automation where you can, but respect where a manual step is acceptable. A great example is continuous delivery vs. continuous deployment. We want everyone to embrace continuous deployment, but respect when a project isn't quite there yet.

### 6. Keep Your Scripts Platform-Agnostic{: name="agnostic"}

My preferred CI engine is GitLab CI. That said, the only platform alignment I build into my code here is AWS. If you want to take these scripts to GitHub actions or CodeShip or AWS CodeBuild or Jenkins, be my guest. Everything in this project group is built so I can package it into a Docker container and run it wherever Docker is available. You can also take the raw scripts and port them wherever you'd like.

Make your software dependencies explicit. If your code can only work on CI Platform X, I'd like to see it, but I probably won't accept a merge request into this group of projects. **If GitLab CI is what you work with, keep reading.**

With GitLab, I can version-control my CI steps along with my code, choose where it runs with GitLab runners, and know when a merge request is ready to ship and when it's not.

To leverage the best features of GitLab CI, jobs and the scripts behind them should be designed to:

* always use instance profiles, never use keys
* run in parallel when possible
* run if they have no upstream dependencies, even if prior jobs fail. Example: I need my CloudFormation templates uploaded to S3 to evaluate a change set, but I don't need them in S3 to run linting jobs locally. `when: always` works great for this.
* one job -> one actionable output. Do one thing and evaluate pass, fail, or warning (`allow_failure: true`)
* abstract artifact build/test/deploy steps using `artifacts` when possible

### 7. DRY Configurations{: name="dry"}

DRY stands for "Don't Repeat Yourself". Any time that a configuration or a line of code is used by multiple projects, it should be abstracted and listed as a dependency if possible. Abstracting and reusing code snippets allows all projects to benefit from improvements to that snippet or project. `include` and `extends` really help accomplish this with GitLab CI. I maintain [templates](templates) so I can keep my .gitlab-ci.yml DRY and reuse ubiquitous CI steps anywhere I want from one file.

## Merge Requests

Merge requests must meet the following criteria:

1. Include a problem description
2. Include a description of the feature or fix being introduced, and any rationale that guided that particular approach
3. Include updates to readme files
4. Pass all existing CI jobs
5. Add or update CI jobs as appropriate
6. Resolve any discussions

The above requirements benefit all of us by ensuring that only high-quality, tested and verified code that executes on our CI/CD tenants is included in our tool set.

## Other Important Considerations

1. Always match naming conventions in unit testing for this project with what the final product will be in the CI templates.
2. Clean up all staged resources.
3. Isolate unit tests based on test criteria. Ensure that no two jobs can collide so the results are accurate and repeatable.

## Naming Conventions

### Jobs

1. Jobs are named with colon-delimited values referred to here as properties.
2. The first property corresponds to the job's stage, e.g. `unit`, `integration`, and `cleanup`.
3. The second property of the job name is the name of the script.
4. The third property of the job name is a descriptor to identify the exact case being tested, if applicable.
5. `cleanup` job first properties are `cleanup:stage` where `stage` is the stage of the job which created the resources to be removed.
