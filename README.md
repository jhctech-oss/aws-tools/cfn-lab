# cfn-lab

cfn-lab is made to support out-of-the-box GitLab CI pipelines for building, testing, and deploying CloudFormation templates. It is made up of two parts:

1. [Templates](templates) which allow a consumer to inherit CI jobs from this project with a simple `include` statement in their GitLab project.
2. A [Docker](Dockerfile) image containing software dependencies for running those jobs.

Implementing this project will yield a CI pipeline that looks something like this (for the lite version):

![Sample GitLab CI lite pipeline](img/Sample-Pipeline-Lite.png "cfn-lab lite pipeline")

Or this (for the full version):

![Sample GitLab CI full pipeline](img/Sample-Pipeline-Full.png "cfn-lab full pipeline")

If you are new to this project, we recommend starting with the [lite version](templates/README.md) of the pipeline.

Check [CONTRIBUTING.md](CONTRIBUTING.md) for the principles that guide the design of modules in this project group.

## Included Software

### Base Image: [`alpine:latest`](https://hub.docker.com/_/alpine)

### Installed Packages

#### External Resources

All credit for the packages below goes to their respective developers.

* [YAML LINT](https://yamllint.readthedocs.io/en/stable/quickstart.html) - Lint tool for YAML
* [Json-spec](https://json-spec.readthedocs.io/) - Lint tool for json
* [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-using.html) - Command line interface for AWS API
* [Python 3](https://www.python.org/) - required for boto3 to run
* [Pip](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-using.html) - required to manage python modules installs
* [Boto3](https://github.com/boto/boto3) - Python SDK for AWS CLI
* [Ruby](https://www.ruby-lang.org/en/documentation/) - required for cfn-nag
* [cfn-nag](https://github.com/stelligent/cfn_nag) - CloudFormation analysis tool for security best practices.
* [cfn-python-lint](https://github.com/awslabs/cfn-python-lint) - CloudFormation linting tool for best practices.
* [cfn-tail](https://github.com/taimos/cfn-tail) - CloudFormation tool for streaming CloudFormation stack events.

#### Custom CI Jobs

This project leverages the following utility application sets:

* [cfn-build](cfn-build/README.md)
* [cfn-test](cfn-test/README.md)
* [cfn-deploy](cfn-deploy/README.md)
* [cfn-utils](cfn-utils/README.md)

All scripts are made available on the system path for execution. Documentation on each can be found in their respective README files. If you are using this project for the first time, we recommend you check out the [CloudFormation CI template](templates/README.md) first to see the understand the CI/CD pipeline flow of which these jobs are part.

### Software Life Cycle

Software in this project is tested to ensure that components function both individually and together as designed. Since the purpose of this project is to test the software, modules may be run in orders other than their intended production purpose. For example, [cfn-build](cfn-build) contains jobs that would run during the `build` stage of a CloudFormation project's software life cycle, but may appear in the `unit test` stage in this project's own [.gitlab-ci.yml](.gitlab-ci.yml) because we need to test the script itself.

1. `pre-build` - building Docker image to satisfy dependencies in downstream jobs
2. `build` - only jobs necessary to facilitate the environment for later testing and deployment jobs
3. `unit test` - jobs designed to test the functionality of _individual_ scripts
4. `integration test` - jobs designed to test the combined functionality of a sequence of scripts
5. `qa` - jobs designed to ensure high-quality code in all non-functional aspects
6. `cleanup` - jobs designed to eliminate any leftover temporary resources from prior stages
7. `deploy` - jobs designed to make the `cfn-lab` image and any associated software packages available for use

## CI Templates

This project contains GitLab CI templates which can be used to jump-start development with this image. These templates are found in [templates](templates/README.md).
