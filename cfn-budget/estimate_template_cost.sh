#!/bin/bash -e
BUCKET=$(echo "$1" | tr '[:upper:]' '[:lower:]'); # ensure lowercase for valid bucket name
PREFIX="$2";

prefixBool=true

if [ "$PREFIX" = "/" ]; then
  prefixBool=false
fi

estimate_cost_command="aws cloudformation estimate-template-cost"

for param_var in ${!CFN_PARAMETER_*}
do
  CFN_PROCESSED_PARAMS="$CFN_PROCESSED_PARAMS ParameterKey=${param_var#CFN_PARAMETER_},ParameterValue='${!param_var}'"
done
if [ -n "$CFN_PROCESSED_PARAMS" ]; then
  estimate_cost_command="$estimate_cost_command --parameters $CFN_PROCESSED_PARAMS"
fi

# Loop through the YAML templates in this repository
if [ "$prefixBool" = true ]; then
  keys=$(aws s3api list-objects-v2 --bucket "$BUCKET" --prefix "$PREFIX" --query "Contents[].Key" --output json)
else
  keys=$(aws s3api list-objects-v2 --bucket "$BUCKET" --query "Contents[].Key" --output json)
fi

JSONARRAY=$(echo "$keys" | jq -r '.[]')
for key in $JSONARRAY; do
    # Call the AWS API to estimate the cost, but only for files with these extensions
    echo "Estimating the cost for $key..."
    if [[ $key =~ \.(template|json|yml|yaml)$ ]]; then
      estimate_cost_command="$estimate_cost_command --template-url https://$BUCKET.s3.amazonaws.com/$key"
      if COST=$(eval "$estimate_cost_command")
      then
        echo "$COST"
      else
        echo "There was an issue estimating the cost for $key."
        exit 1
      fi
    fi
done
