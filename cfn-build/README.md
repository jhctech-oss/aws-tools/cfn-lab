# cfn-build

CloudFormation utilities for the `build` phase of a CloudFormation CI/CD pipeline.

## Software Dependencies

1. awscli

## Scripts

Scripts are categorized with a type and a stage. The definitions are below:

### Types

* **Type:** CI: this script is primarily used to execute CI jobs by executing the script itself
* **Type:** Utility: this script is used by one or multiple `CI` or `Utility` scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

### Stages

### List of Scripts

#### [cleanup_bucket.sh](cleanup_bucket.sh)

Used to remove an S3 bucket and all objects contained within. Most useful for cleaning up buckets which were used as temporary staging locations for tests like `validate-template`.
**Type**: CI

##### Required Parameters for cleanup_bucket

None

##### Required Command Line Arguments for cleanup_bucket

1. the name of the S3 bucket to be deleted
