#!/bin/bash -e

BUCKET_NAME=$(echo "$1" | tr '[:upper:]' '[:lower:]');
PREFIX="$2";
TEMPLATE_PATH="$3";
BASE_TEMPLATE_NAME="$4";

prefixBool=true

if [ "${PREFIX: -1}" == "/" ] && [ ${#PREFIX} -ge 1 ]; then
  PKG_PREFIX="${PREFIX::-1}"
elif [ ${#PREFIX} -eq 1 ]; then
  prefixBool=false
else
  PKG_PREFIX="$PREFIX"
fi

if ! aws s3api head-bucket --bucket "$BUCKET_NAME" 2>/dev/null; then
  echo "$BUCKET_NAME does not exist. Creating..."
  aws s3 mb s3://"$BUCKET_NAME"
fi

cd "$TEMPLATE_PATH"

find . -type f -not -name "$BASE_TEMPLATE_NAME"|while read -r fileName; do
  if [ "$prefixBool" = true ]; then
    aws cloudformation package \
      --template-file "$fileName" \
      --s3-bucket "$BUCKET_NAME" \
      --s3-prefix "$PKG_PREFIX" \
      --output-template-file ../packaged-"$(basename "$fileName")"
  else
    aws cloudformation package \
      --template-file "$fileName" \
      --s3-bucket "$BUCKET_NAME" \
      --output-template-file ../packaged-"$(basename "$fileName")"
  fi
done

if [ "$prefixBool" = true ]; then
  aws cloudformation package \
    --template-file "$BASE_TEMPLATE_NAME" \
    --s3-bucket "$BUCKET_NAME" \
    --s3-prefix "$PKG_PREFIX" \
    --output-template-file ../packaged-"$BASE_TEMPLATE_NAME"
    # [--kms-key-id <value>]
    # [--force-upload]
    # [--metadata <value>]
else
  aws cloudformation package \
    --template-file "$BASE_TEMPLATE_NAME" \
    --s3-bucket "$BUCKET_NAME" \
    --output-template-file ../packaged-"$BASE_TEMPLATE_NAME"
    # [--kms-key-id <value>]
    # [--force-upload]
    # [--metadata <value>]
fi

cd ..
aws s3 cp packaged-"${BASE_TEMPLATE_NAME}" s3://"${BUCKET_NAME}""${PREFIX}""${BASE_TEMPLATE_NAME}"
