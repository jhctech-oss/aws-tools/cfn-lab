#!/bin/bash -e

BUCKET_NAME=$(echo "$1" | tr '[:upper:]' '[:lower:]');

if ! aws s3api head-bucket --bucket "$BUCKET_NAME" 2>/dev/null; then
  echo "$BUCKET_NAME does not exist. Nothing to clean up."
else
  aws s3 rm s3://$BUCKET_NAME/ --recursive
  aws s3 rb s3://$BUCKET_NAME
fi
