#!/bin/bash -e

BUCKET_NAME=$(echo "$1" | tr '[:upper:]' '[:lower:]');
PREFIX="$2";
# Drop the first two arguments
shift;
shift;
TEMPLATE_PATHS="$@"; # all remaining command line arguments

if ! aws s3api head-bucket --bucket "$BUCKET_NAME" 2>/dev/null; then
  echo "$BUCKET_NAME does not exist. Creating..."
  aws s3 mb s3://$BUCKET_NAME
fi

echo "Uploading files from paths $TEMPLATE_PATHS to s3://$BUCKET_NAME..."
for path in $TEMPLATE_PATHS
do
  if [ -d "$path" ]
  then
    copyCommand="aws s3 cp $path/ s3://${BUCKET_NAME}${PREFIX} --recursive"
    echo $copyCommand
    eval $copyCommand
  else
    copyCommand="aws s3 cp $path s3://${BUCKET_NAME}${PREFIX}${path}"
    echo $copyCommand
    eval $copyCommand
  fi
done
