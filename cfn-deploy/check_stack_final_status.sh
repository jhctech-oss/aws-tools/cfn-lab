#!/bin/bash -e

if [ -e cfn-deploy/get_stack_status.sh ]; then
  # shellcheck disable=SC1091
  . cfn-deploy/get_stack_status.sh
else
  # shellcheck disable=SC1091
  . /usr/bin/get_stack_status
fi

check_stack_final_status() {
  get_stack_status
  # shellcheck disable=SC2154
  case $stack_status in
  # Successful end states
  "CREATE_COMPLETE")
    echo "$CFN_STACK_NAME has been successfully created."
    ;;
  "UPDATE_COMPLETE")
    echo "$CFN_STACK_NAME has been successfully updated."
    ;;
  "DELETE_COMPLETE")
    echo "$CFN_STACK_NAME has been successfully deleted."
    ;;
  "REVIEW_IN_PROGRESS")
    echo "$CFN_STACK_NAME is waiting to be created."
    ;;
  # Failed end states; let the calling function decide what to do
  "UPDATE_ROLLBACK_COMPLETE"|"UPDATE_ROLLBACK_FAILED" \
  |"ROLLBACK_COMPLETE"|"CREATE_FAILED"|"ROLLBACK_FAILED"|"DELETE_FAILED")
    echo "$CFN_STACK_NAME is in failed end state $stack_status."
    ;;
  # Non end states
  "CREATE_IN_PROGRESS"|"DELETE_IN_PROGRESS" \
  |"ROLLBACK_IN_PROGRESS"|"UPDATE_ROLLBACK_IN_PROGRESS" \
  |"UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS"|"UPDATE_IN_PROGRESS" \
  |"UPDATE_COMPLETE_CLEANUP_IN_PROGRESS")
    # we should never get here because of cloudformation_tail, but in case we do...
    echo "$CFN_STACK_NAME is in non-end state $stack_status."
    exit 1
    ;;
  *)
    # in case there's some state not listed here, let the calling function decide what to do
    echo "$CFN_STACK_NAME is in unknown state $stack_status."
    ;;
  esac
}
