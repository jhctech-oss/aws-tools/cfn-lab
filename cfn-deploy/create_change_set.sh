#!/bin/bash

if [ -e cfn-deploy/get_stack_status.sh ]; then
  # shellcheck disable=SC1091
  . cfn-deploy/check_stack_final_status.sh
  # shellcheck disable=SC1091
  . cfn-deploy/delete_stack.sh
  # shellcheck disable=SC1091
  . cfn-deploy/get_stack_status.sh
  # shellcheck disable=SC1091
  . cfn-deploy/detect_changes.sh
else
  # shellcheck disable=SC1091
  . /usr/bin/check_stack_final_status
  # shellcheck disable=SC1091
  . /usr/bin/delete_stack
  # shellcheck disable=SC1091
  . /usr/bin/get_stack_status
  # shellcheck disable=SC1091
  . /usr/bin/detect_changes
fi

## Required variables
# CFN_BASE_TEMPLATE
# CFN_STACK_NAME
# CFN_CHANGE_SET_NAME
## Optional variables
# CFN_ROLE_ARN
# CFN_CAPABILITIES
# CFN_CHANGE_SET_DESCRIPTION
# CFN_PARAMETER_*
# CFN_PROCESSED_PARAMS
## Options not implemented
# [--use-previous-template | --no-use-previous-template]
# [--notification-arns <value>]
# [--client-token <value>]
# [--cli-input-json <value>]
# [--generate-cli-skeleton <value>]

create_change_set_command="aws cloudformation create-change-set --stack-name $CFN_STACK_NAME"
if [[ $CFN_BASE_TEMPLATE == https://*.s3* ]]; then
  create_change_set_command="$create_change_set_command --template-url $CFN_BASE_TEMPLATE"
else
    create_change_set_command="$create_change_set_command --template-body file://$CFN_BASE_TEMPLATE"
fi
create_change_set_command="$create_change_set_command --change-set-name $CFN_CHANGE_SET_NAME"
if [ -n "$CFN_ROLE_ARN" ]; then
  create_change_set_command="$create_change_set_command --role-arn $CFN_ROLE_ARN"
fi
if [ -n "$CFN_CAPABILITIES" ]; then
  create_change_set_command="$create_change_set_command --capabilities $CFN_CAPABILITIES"
fi
if [ -n "$CFN_CHANGE_SET_DESCRIPTION" ]; then
  create_change_set_command="$create_change_set_command --description $CFN_CHANGE_SET_DESCRIPTION"
fi
if [ -s "$PWD"/cfn_tags.json ]; then
  create_change_set_command="$create_change_set_command --tags file://cfn_tags.json"
fi
if [ -s "$PWD"/cfn_rollback_configuration.json ]; then
  create_change_set_command="$create_change_set_command --rollback-configuration file://cfn_rollback_configuration.json"
fi
if [ -s "$PWD"/cfn_resource_types.txt ]; then
  create_change_set_command="$create_change_set_command --resource-types file://cfn_resource_types.txt"
fi
for param_var in ${!CFN_PARAMETER_*}
do
  CFN_PROCESSED_PARAMS="$CFN_PROCESSED_PARAMS ParameterKey=${param_var#CFN_PARAMETER_},ParameterValue=\\\"${!param_var}\\\""
done
if [ -n "$CFN_PROCESSED_PARAMS" ]; then
  create_change_set_command="$create_change_set_command --parameters $CFN_PROCESSED_PARAMS"
fi

if get_stack_status
then
  # shellcheck disable=SC2154
  case $stack_status in
    "CREATE_COMPLETE"|"UPDATE_COMPLETE"|"UPDATE_ROLLBACK_COMPLETE")
      CFN_CHANGE_SET_TYPE="UPDATE"
      ;;
    "UPDATE_ROLLBACK_FAILED")
      aws cloudformation continue-update-rollback --stack-name "$CFN_STACK_NAME" || exit 1
      cfn-tail "$CFN_STACK_NAME"
      rollback_status=$(eval "$get_status")
      case $rollback_status in
        "UPDATE_ROLLBACK_COMPLETE")
          CFN_CHANGE_SET_TYPE="UPDATE"
          ;;
        *)
          check_stack_final_status
          exit 1
          ;;
      esac
      ;;
    "DELETE_COMPLETE")
      CFN_CHANGE_SET_TYPE="CREATE"
      ;;
    # These states all represent original stack creation that failed. Since no resources will be present, it is safe to delete the stack
    "ROLLBACK_COMPLETE"|"CREATE_FAILED"|"ROLLBACK_FAILED")
      echo "$CFN_STACK_NAME status is $stack_status. Deleting stack before proceeding..."
      aws cloudformation update-termination-protection --no-enable-termination-protection --stack-name "$CFN_STACK_NAME"
      delete_stack
      CFN_CHANGE_SET_TYPE="CREATE"
      ;;
    # Neither stack create nor stack update can be executed in these states
    "CREATE_IN_PROGRESS"|"DELETE_IN_PROGRESS" \
    |"REVIEW_IN_PROGRESS"|"ROLLBACK_IN_PROGRESS"|"UPDATE_ROLLBACK_IN_PROGRESS" \
    |"UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS"|"UPDATE_IN_PROGRESS" \
    |"UPDATE_COMPLETE_CLEANUP_IN_PROGRESS"|"DELETE_FAILED")
      check_stack_final_status
      exit 1
      ;;
    *)
      CFN_CHANGE_SET_TYPE="CREATE"
      ;;
  esac
else
  echo "Stack does not exist. Executing with type CREATE..."
  CFN_CHANGE_SET_TYPE="CREATE"
fi

create_change_set_command="$create_change_set_command --change-set-type $CFN_CHANGE_SET_TYPE"
if [ "$CI_DEBUG_TRACE" == "true" ]; then
  echo "$create_change_set_command"
fi
if eval "$create_change_set_command"
then
  echo "Change set was created."
else
  echo "Change set could not be created."
  exit 1
fi
{ # try
  aws cloudformation wait change-set-create-complete \
  --stack-name "$CFN_STACK_NAME" \
  --change-set-name "$CFN_CHANGE_SET_NAME"
} || { # catch
    # do not exit on a failed wait. Most likely there are no changes.
  echo "Waiter encountered error."
}

detect_changes
