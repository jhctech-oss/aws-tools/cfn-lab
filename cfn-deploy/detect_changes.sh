#!/bin/bash -e

detect_changes() {
  aws cloudformation describe-change-set \
    --stack-name "$CFN_STACK_NAME" \
    --change-set-name "$CFN_CHANGE_SET_NAME" > changeSetDetails.json
  cat changeSetDetails.json > /dev/stderr
  cat changeSetDetails.json | jq '.StatusReason' > statusReason.json
  echo "Status reason: " | cat - statusReason.json > /dev/stderr
  cat changeSetDetails.json | jq '.Changes' > changes.json
  if grep "The submitted information didn't contain changes." statusReason.json > /dev/null; then
    echo "Change set did not contain changes. Exiting..." > /dev/stderr
    exitCode=0
  elif [[ $(< changes.json) == "[]" ]]; then
    echo "Change set did not contain changes. Exiting..." > /dev/stderr
    exitCode=0
  else
    echo "Change set contains changes. Review before deploying." > /dev/stderr
    cat changes.json | jq '.[] | select(.ResourceChange.Action == "Add")' --join-output > additions.json
    cat changes.json | jq '.[] | select(.ResourceChange.Action == "Remove")' --join-output > removals.json
    cat changes.json | jq '.[] | select(.ResourceChange.Action == "Modify")' --join-output > modifications.json
    cat changes.json | jq '.[] | select(.ResourceChange.Action == "Import")' --join-output > imports.json
  fi
}
