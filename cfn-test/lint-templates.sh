#!/bin/bash
ERROR_COUNT=0;
TEMPLATE_PATHS='.'
OPTIONS=""

usage() { echo "Usage: $0 [ -t [ template-paths... ] ] [ -o options ]" 1>&2; }

while getopts "t:o:" option; do
    case "${option}" in
        t) TEMPLATE_PATHS=${OPTARG} ;;
        o) OPTIONS=${OPTARG} ;;
        ?) usage; exit 1;;
    esac
done
printf "Lint CloudFormation templates..."

for FILE in $(find "$TEMPLATE_PATHS" -regex ".*\.\(json\|y[a]*ml\|template\)"); do
  # Validate the template with cfn-lint
  if ERRORS=$(cfn-lint --format quiet $OPTIONS $FILE); then
    printf "\\n\\n[pass] %s" "$FILE";
  else
    ((ERROR_COUNT++));
    printf "\\n\\n[fail] %s" "$FILE";
    printf "\\n%s" "$ERRORS";
  fi;
done;

printf "\\n\\n%s file(s) with cfn-lint error(s)\n" "$ERROR_COUNT";
if [ "$ERROR_COUNT" -gt 0 ];
  then exit 1;
fi
