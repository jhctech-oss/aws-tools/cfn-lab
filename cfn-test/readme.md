# CloudFormation Tests

Contains scripts for performing CloudFormation `test`s.

## Software Dependencies

* awscli
* [yamllint](https://github.com/adrienverge/yamllint)
* [jq](https://github.com/stedolan/jq)

## Scripts

Scripts are categorized with a type and a stage. The definitions are below:

### Types

* **Type:** CI: this script is primarily used to execute CI jobs by executing the script itself
* **Type:** Utility: this script is used by one or multiple `CI` or `Utility` scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

### Stages

* `unit test` - jobs designed to test the functionality of _individual_ scripts
* `qa` - jobs designed to ensure high-quality code in all non-functional aspects

### List of Scripts

#### [lint-templates.sh](lint-templates.sh)

Checks for cloudformation syntax errors in files with extensions `.json`, `.yaml`, `.yml`, and `.template`.
**Type:** CI
**Stage:** `qa`

##### Required Parameters for lint-templates

None

##### Required Command Line Arguments for lint-templates

None

##### Optional Command Line Arguments for lint-templates

* -t
  * *List of template paths. Accepts directories and files. If not specified utilizes the local `.` directory. Note that all sub-directories are also searched for applicable files.*
* -o
  * *Options to pass to cfn-lint*

#### [validate-json.sh](validate-json.sh)

Checks for JSON syntax errors in files with extension `.json` using `jq`.
**Type:** CI
**Stage:** `qa`

##### Required Parameters for validate-json

None

##### Required Command Line Arguments for validate-json

None

#### [validate-yaml.sh](validate-yaml.sh)

Uses `yamllint` to lint files with extensions `.yml` and `.yaml` for syntax errors.
**Type:** CI
**Stage:** `qa`

##### Required Parameters for validate-yaml

None

##### Required Command Line Arguments for validate-yaml

None
