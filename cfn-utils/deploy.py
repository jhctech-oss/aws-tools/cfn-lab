#! /usr/bin/python3
"""CloudFormation Stack and stack set classes"""

import re
import sys
from subprocess import call
from time import sleep
import json
import os
import boto3
from botocore.exceptions import ClientError
import text_colors

CLIENT = boto3.client('cloudformation')
PWD = '/builds/jhctechnology/aws-cloudformation-utilities/' + \
      'awslint/.git/'
TABLE_CHARACTER_WIDTH = 284
SLEEP_SECONDS = 10


class Stack:
    """A CloudFormation stack"""
    def __init__(self, stack_name):
        self.stack_name = stack_name
        self.stack_status = self.get_stack_status()

    def check_for_drift(self):
        """Perform drift detection on a CloudFormation stack"""
        drift_detection_response = CLIENT.detect_stack_drift(
            StackName=self.stack_name
        )
        drift_detection_status = 'undefined'
        while drift_detection_status != 'DETECTION_COMPLETE':
            drift_detection_status = \
                CLIENT.describe_stack_drift_detection_status(
                    StackDriftDetectionId=drift_detection_response[
                        "StackDriftDetectionId"]
                )["DetectionStatus"]
            if drift_detection_status == 'DETECTION_FAILED':
                print("Drift detection encountered an error.")
                sys.exit(1)
            sleep(10)
            drifts = CLIENT.describe_stack_resource_drifts(
                StackName=self.stack_name,
                StackResourceDriftStatusFilters=[
                    'MODIFIED',
                    'DELETED'
                ]
            )
            print(drifts)
            return drifts

    @staticmethod
    def drift_detection_error():
        """Error handling during drift detection"""
        print('Stack is not in a healthy/final state. ' +
              'Please restore the stack to CREATE_COMPLETE, ' +
              'UPDATE_COMPLETE, UPDATE_ROLLBACK_COMPLETE ' +
              'before attempting to detect drift.')
        exit(1)

    @staticmethod
    def no_drift_possible():
        """No drift possible special case handling"""
        print('Stack does not exist. No resource drift is possible.')
        exit()

    def get_stack_status(self):
        """Get a CloudFormation stack's current status."""
        try:
            response = CLIENT.describe_stacks(StackName=self.stack_name)
            self.stack_status = response["Stacks"][0]["StackStatus"]
        except ClientError:
            # use DELETE_COMPLETE as both DELETE_COMPLETE and undefined
            self.stack_status = 'DELETE_COMPLETE'
        return self.stack_status

    def check_stack_final_status(self):
        """Check if a CloudFormation stack is currently in an end state."""
        stack_status = self.get_stack_status()
        # Successful end states
        if stack_status == "CREATE_COMPLETE":
            print(self.stack_name, 'has been successfully created.')
        elif stack_status == "UPDATE_COMPLETE":
            print(self.stack_name, 'has been successfully updated.')
        elif stack_status == 'DELETE_COMPLETE':
            print(self.stack_name, 'has been deleted or does not exist.')
        elif stack_status == 'REVIEW_IN_PROGRESS':
            print(self.stack_name, 'is waiting to be created.')
        # Failed end states; let the calling function decide what to do
        elif stack_status in [
                'ROLLBACK_COMPLETE', 'UPDATE_ROLLBACK_COMPLETE',
                'CREATE_FAILED', 'ROLLBACK_FAILED', 'UPDATE_ROLLBACK_FAILED',
                'DELETE_FAILED'
        ]:
            print(self.stack_name, ' is in failed end state ', stack_status)
        # Non end states
        elif stack_status in [
                'CREATE_IN_PROGRESS', 'UPDATE_IN_PROGRESS',
                'DELETE_IN_PROGRESS',
                'ROLLBACK_IN_PROGRESS', 'UPDATE_ROLLBACK_IN_PROGRESS',
                'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS',
                'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS'
        ]:
            # we should never get here because of cloudformation_tail,
            # but in case we do...
            print(self.stack_name, ' is in non-end state ', stack_status)
            exit(1)
        else:
            # in case there's some state not listed here,
            # let the calling function decide what to do
            print(self.stack_name, 'is in unknown state', stack_status)

    def create_change_set(self,
                          base_template,
                          change_set_name,
                          capabilities='None',
                          change_set_description='None',
                          role_arn='None'):
        """Create a change set for a CloudFormation stack."""

        create_change_set_command = {
            'ChangeSetName': change_set_name,
            'StackName': self.stack_name
        }
        regexp = re.compile(r'https://.*\.s3.*')
        if regexp.search(base_template):
            create_change_set_command['TemplateURL'] = base_template
        else:
            create_change_set_command['TemplateBody'] = base_template
        if role_arn != 'None':
            create_change_set_command['RoleARN'] = role_arn
        if capabilities != 'None':
            i = 0
            create_change_set_command['Capabilities'] = []
            for capability in capabilities:
                create_change_set_command['Capabilities'][i] = capability
                i += 1
        if change_set_description != 'None':
            create_change_set_command['Description'] = change_set_description
        try:
            tags_file = open(PWD + 'cfn_tags.json')
            create_change_set_command['Tags'] = tags_file
            tags_file.close()
        except FileNotFoundError:
            pass
        try:
            rollback_config_file = open(
                PWD + 'cfn_rollback_configuration.json')
            create_change_set_command[
                'RollbackConfiguration'] = rollback_config_file
            rollback_config_file.close()
        except FileNotFoundError:
            pass
        try:
            resource_types_file = open(PWD + 'cfn_resource_types.txt')
            create_change_set_command['ResourceTypes'] = resource_types_file
            resource_types_file.close()
        except FileNotFoundError:
            pass

    def delete_stack(self):
        """Delete a CloudFormation stack."""
        self.get_stack_status()
        if self.stack_status in [
                'CREATE_COMPLETE', 'UPDATE_COMPLETE', 'ROLLBACK_COMPLETE',
                'UPDATE_ROLLBACK_COMPLETE',
                'REVIEW_IN_PROGRESS',
                'UPDATE_ROLLBACK_FAILED', 'CREATE_FAILED', 'ROLLBACK_FAILED',
                'DELETE_FAILED'
        ]:
            CLIENT.update_termination_protection(
                EnableTerminationProtection=False,
                StackName=self.stack_name
            )
            CLIENT.delete_stack(
                StackName=self.stack_name
            )
            call(['cfn-tail', self.stack_name])
            self.check_stack_final_status()
        else:
            print('It looks like that stack is already deleted ' +
                  'or does not exist.')

    def delete_change_set(self, change_set_name):
        """Delete a change set for a CloudFormation stack."""
        print('Deleting the change set', change_set_name, 'from', self.stack_name)
        response = CLIENT.delete_change_set(
            ChangeSetName=change_set_name,
            StackName=self.stack_name
        )
        print(response)
        self.get_stack_status()
        if self.stack_status in ['REVIEW_IN_PROGRESS']:
            # delete REVIEW_IN_PROGRESS stacks
            print(self.stack_name, 'will be deleted since it is in the REVIEW_IN_PROGRESS state.')
            self.delete_stack()

    def execute_change_set(self, change_set_name):
        """Execute a change set for a CloudFormation stack."""
        # Check that change set exists and is not failed
        response = CLIENT.describe_change_set(
            ChangeSetName=change_set_name,
            StackName=self.stack_name
        )
        if response['Status'] != 'FAILED':
            CLIENT.execute_change_set(
                ChangeSetName=change_set_name,
                StackName=self.stack_name
            )
            call(['cfn-tail', self.stack_name])
            self.get_stack_status()
            if self.stack_status in ['CREATE_COMPLETE', 'UPDATE_COMPLETE']:
                print('The stack operation completed successfully.')
            else:
                print('The stack operation was not completed. ' +
                      'Stack reports status:', self.stack_status)
                exit(1)
        elif "The submitted information didn't contain changes." in response['StatusReason']:
            print(response['StatusReason'])
            CLIENT.delete_change_set(
                ChangeSetName=change_set_name,
                StackName=self.stack_name
            )
        else:
            print(response['StatusReason'])
            exit(1)


class StackSet:
    """A CloudFormation stack set"""

    END_STATES = ['SUCCEEDED', 'FAILED', 'STOPPED']
    FAILED_END_STATES = ['FAILED', 'STOPPED']
    SUCCESSFUL_END_STATES = ['SUCCEEDED']

    def __init__(self, stack_set_name):
        self.stack_set_name = stack_set_name
        self.stack_set_status = self.get_stack_set_status()

    def get_stack_set_status(self):
        """Get a CloudFormation stack set's current status."""
        try:
            response = CLIENT.describe_stack_set(StackSetName=self.stack_set_name)
            self.stack_set_status = response["StackSet"]["Status"]
        except ClientError:
            # use DELETED as both DELETED and undefined
            self.stack_set_status = 'DELETED'
        return self.stack_set_status

    @staticmethod
    def colorize_operation_status(status):
        """Add color to operation statuses."""
        if status in ['RUNNING', 'SUCCEEDED']:
            return text_colors.OK_GREEN + status + text_colors.END_COLOR
        if status in ['FAILED', 'STOPPING', 'STOPPED']:
            return text_colors.FAIL + status + text_colors.END_COLOR
        return status  # some unknown status, skip color

    @staticmethod
    def colorize_stack_instance_status(status):
        """Add color to stack instance statuses."""
        # other non-final states: 'PENDING'|'RUNNING'
        if status == 'SUCCEEDED':
            return text_colors.OK_GREEN + status + text_colors.END_COLOR
        if status in ['FAILED', 'CANCELLED']:
            return text_colors.FAIL + status + text_colors.END_COLOR
        return status  # some unknown status, skip color

    @staticmethod
    def convert_comma_space_separated_values_to_list(unformatted_string):
        """Convert a comma or comma-space delimited string to a list."""
        return [entry.strip() for entry in unformatted_string.split(',')]

    def create_stack_instances(self,
                               accounts,
                               regions,
                               operation_id,
                               operation_preferences):
        """Create stack instances"""
        response = CLIENT.create_stack_instances(
            StackSetName=self.stack_set_name,
            Accounts=accounts,
            Regions=regions,
            # ParameterOverrides=[
            #     {
            #         'ParameterKey': 'string',
            #         'ParameterValue': 'string',
            #         'UsePreviousValue': True|False,
            #         'ResolvedValue': 'string'
            #     },
            # ],
            OperationPreferences=operation_preferences,
            OperationId=operation_id
        )
        return response

    def delete_stack_set(self,
                         operation_preferences='MaxConcurrentPercentage=100,FailureToleranceCount=0'):
        """Deletes a stack set and all of of its stack instances."""
        # make sure stack set is in an end state with no running operations
        self.wait_operation_complete()
        # delete all stack instances
        current_stack_instances = self.get_current_stack_instances()
        response = CLIENT.delete_stack_instances(
            StackSetName=self.stack_set_name,
            Accounts=current_stack_instances['Accounts'],
            Regions=current_stack_instances['Regions'],
            OperationPreferences=self.format_operation_preferences(operation_preferences),
            RetainStacks=False,
            OperationId=self.generate_operation_id('delete-all')
        )
        if self.tail_operation(response):
            # delete the stack set
            response = CLIENT.delete_stack_set(
                StackSetName=self.stack_set_name
            )
            print('Stack set', self.stack_set_name, 'successfully deleted.')
            exit()

    def deploy_stack_set(self,
                         base_template,
                         # accounts,
                         # regions,
                         capabilities='None',
                         description='No description given',
                         operation_preferences='MaxConcurrentPercentage=50,FailureTolerancePercentage=10'):
        """Perform a create/update or update action on a stack set."""

        # Initial data transformations
        # accounts = self.convert_comma_space_separated_values_to_list(accounts)
        # regions = self.convert_comma_space_separated_values_to_list(regions)
        operation_preferences = self.format_operation_preferences(operation_preferences)

        deploy_stack_set_command = {
            'StackSetName': self.stack_set_name,
            'Description': description
        }

        regexp = re.compile(r'https://.*\.s3.*')
        if regexp.search(base_template):
            deploy_stack_set_command['TemplateURL'] = base_template
        else:
            deploy_stack_set_command['TemplateBody'] = base_template
        if capabilities != 'None':
            capabilities = capabilities.split()
            deploy_stack_set_command['Capabilities'] = []
            for capability in capabilities:
                deploy_stack_set_command['Capabilities'].append(capability)
        try:
            with open('cfn_tags.json', 'r') as tag_file:
                deploy_stack_set_command['Tags'] = json.load(tag_file)
        except FileNotFoundError:
            pass
        cfn_processed_params = []
        if 'CFN_PROCESSED_PARAMS' in os.environ:
            cfn_processed_params = os.getenv('CFN_PROCESSED_PARAMS')
        else:
            pat = re.compile('CFN_PARAMETER_*')
            for can in os.environ:
                if pat.search(can):
                    cfn_processed_params.append({
                        'ParameterKey': can[14:],
                        'ParameterValue': os.getenv(can)
                    })
        deploy_stack_set_command['Parameters'] = cfn_processed_params
        self.get_stack_set_status()
        if self.stack_set_status == 'DELETED':
            # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.create_stack_set
            response = CLIENT.create_stack_set(**deploy_stack_set_command)
            # AdministrationRoleARN='string',
            # ExecutionRoleName='string',
            # ClientRequestToken='string'
            print(response)
            # response = self.create_stack_instances(
            #    accounts, regions, self.generate_operation_id('create'), operation_preferences)
            # if self.tail_operation(response):
            #    print('Stack set and stack instance creation complete.')
            #    exit()
        else:
            # determine which stack instances we need to delete
            # current_stack_instances = self.get_current_stack_instances()
            # accounts_to_delete = []
            # regions_to_delete = []
            # accounts_to_create = []
            # regions_to_create = []
            # for account in current_stack_instances['Accounts']:
            #     if account not in accounts:
            #         accounts_to_delete.append(account)
            # for region in current_stack_instances['Regions']:
            #     if region not in regions:
            #         regions_to_delete.append(region)
            # if accounts_to_delete:
            #     response = CLIENT.delete_stack_instances(
            #         StackSetName=self.stack_set_name,
            #         Accounts=accounts_to_delete,
            #         Regions=current_stack_instances['Regions'],
            #         OperationPreferences=operation_preferences,
            #         RetainStacks=False,
            #         OperationId=self.generate_operation_id('delete-accounts')
            #     )
            #     self.tail_operation(response)
            # self.wait_operation_complete()
            # if regions_to_delete:
            #     response = CLIENT.delete_stack_instances(
            #         StackSetName=self.stack_set_name,
            #         Accounts=current_stack_instances['Accounts'],
            #         Regions=regions_to_delete,
            #         OperationPreferences=operation_preferences,
            #         RetainStacks=False,
            #         OperationId=self.generate_operation_id('delete-regions')
            #     )
            #     self.tail_operation(response)
            # self.wait_operation_complete()
            deploy_stack_set_command['OperationPreferences'] = operation_preferences
            deploy_stack_set_command['UsePreviousTemplate'] = False
            deploy_stack_set_command['OperationId'] = self.generate_operation_id('update')
            # update all stack instances by omitting these
            # deploy_stack_set_command['Accounts'] = accounts
            # deploy_stack_set_command['Regions'] = regions
            # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html#CloudFormation.Client.update_stack_set
            response = CLIENT.update_stack_set(**deploy_stack_set_command)
            # AdministrationRoleARN='string',
            # ExecutionRoleName='string',
            if self.tail_operation(response):
                # determine which stack instances we need to create
                # for account in accounts:
                #     if account not in current_stack_instances['Accounts']:
                #         accounts_to_create.append(account)
                # if accounts_to_create:
                #     response = self.create_stack_instances(
                #         accounts_to_create, regions, self.generate_operation_id('create-accounts'), operation_preferences)
                #     self.tail_operation(response)
                # self.wait_operation_complete()
                # for region in regions:
                #     if region not in current_stack_instances['Regions']:
                #         regions_to_create.append(region)
                # if regions_to_create:
                #     response = self.create_stack_instances(
                #         accounts, regions_to_create, self.generate_operation_id('create-regions'), operation_preferences)
                #     self.tail_operation(response)
                # self.wait_operation_complete()
                print('Stack set update completed successfully.')
                exit()

    @staticmethod
    def format_operation_preferences(operation_preferences):
        """Format operation preferences string from file or command line
        as a dict for stack set actions."""
        try:
            with open('cfn_operation_preferences.json', 'r') as op_file:
                operation_preferences_formatted = json.load(op_file)
        except FileNotFoundError:
            operation_preferences_formatted = dict(
                item.split("=") for item in operation_preferences.split(","))
            # For int values, cast values from strings to ints
            for key in [
                    'FailureToleranceCount',
                    'FailureTolerancePercentage',
                    'MaxConcurrentCount',
                    'MaxConcurrentPercentage'
            ]:
                if key in operation_preferences_formatted:
                    operation_preferences_formatted[key] = int(operation_preferences_formatted[key])
        return operation_preferences_formatted

    @staticmethod
    def generate_operation_id(suffix=''):
        """Generate a unique operation Id based on the GitLab pipeline properties
        to serve as an idempotency token. Append an optional suffix"""
        operation_id = '-'.join([
            os.getenv('CI_SERVER_HOST').replace('.', '-'),
            os.getenv('CI_PROJECT_PATH_SLUG'),
            os.getenv('CI_PIPELINE_IID')
        ])
        if not suffix:
            return operation_id
        return operation_id + '-' + suffix

    def get_current_stack_instances(self):
        """Returns a dict of current stack instance accounts and regions."""
        current_stack_instances = CLIENT.list_stack_instances(
            StackSetName=self.stack_set_name,
        )
        current_stack_instances_formatted = {
            'Accounts': [],
            'Regions': []
        }
        for instance in current_stack_instances['Summaries']:
            current_stack_instances_formatted['Accounts'].append(instance['Account'])
            current_stack_instances_formatted['Regions'].append(instance['Region'])
        return current_stack_instances_formatted

    @staticmethod
    def pad_right(string, size):
        """Pad a string's right side with whitespace up to the specified size."""
        pad = ' ' * size
        if not string:
            return pad
        return (string + pad)[0:len(pad)]

    def tail_operation(self, response):
        """Poll for new stack set operation events until the operation is complete,
        output the results, and return True if it completed successfully."""
        print(response)
        operation_id = response['OperationId']
        operation_final_status = self.wait_operation_complete(True, operation_id)
        response = CLIENT.list_stack_set_operation_results(
            StackSetName=self.stack_set_name,
            OperationId=operation_id
        )
        print('|', ' | '.join([
            self.pad_right('Account', 12),
            self.pad_right('Region', 20),
            self.pad_right('Status', 15),
            self.pad_right('Status Reason', 224)
        ]), '|')
        for summary in response['Summaries']:
            result_entry = '| ' + ' | '.join([
                self.pad_right(summary['Account'], 12),
                self.pad_right(summary['Region'], 20),
                self.colorize_stack_instance_status(summary['Status']) + self.pad_right('', 15 - len(summary['Status']))
            ]) + ' | '
            try:
                result_entry += self.pad_right(summary['StatusReason'], 224)
            except KeyError:
                result_entry += self.pad_right("No reason given.", 224)
            result_entry += ' |'
            print(result_entry)
        print('-' * TABLE_CHARACTER_WIDTH)
        if operation_final_status in self.FAILED_END_STATES:
            print('The stack set operation encountered an error.')
            exit(1)
        elif operation_final_status in self.SUCCESSFUL_END_STATES:
            print('The stack set operation completed successfully.')
            return True
        else:
            # some unknown state; assume failed
            print('The stack set is in unknown state', operation_final_status)
            exit(1)

    def wait_operation_complete(self, send_heartbeat=False, operation_id=None):
        """Check if an operation is currently processing, and wait if it is.
        Also takes a known operation id.
        Optionally sends a heartbeat signal."""
        if operation_id is None:
            response = CLIENT.list_stack_set_operations(
                StackSetName=self.stack_set_name,
                MaxResults=1  # we should only need one since only one operation can be performed at a time
            )
            if not response['Summaries']:
                return True
            operation_id = response['Summaries'][0]['OperationId']
        operation_final_status = ''
        operation_last_status = ''
        heartbeat_timer = 0
        heartbeat_frequency_seconds = 30
        if send_heartbeat:
            print('-' * TABLE_CHARACTER_WIDTH)
            print('| Stack:',
                  self.pad_right(self.stack_set_name, 128),
                  '| Operation Id:',
                  self.pad_right(operation_id, 128),
                  '|')
            print('-' * TABLE_CHARACTER_WIDTH)
            print('|', ' | '.join([
                self.pad_right('Date/Time', 20),
                self.pad_right('Operation Id', 128),
                self.pad_right('Action', 6),
                self.pad_right('Status', 15),
                self.pad_right('Execution Role Name', 99)
            ]), '|')
        else:
            print('Waiting for', operation_id, 'to complete...')
        while True:
            response = CLIENT.describe_stack_set_operation(
                StackSetName=self.stack_set_name,
                OperationId=operation_id
            )
            operation_current_status = response['StackSetOperation']['Status']
            if send_heartbeat:
                # Output status only if different from previous or if heartbeat timer is up
                if (operation_current_status != operation_last_status or
                        heartbeat_timer >= heartbeat_frequency_seconds):
                    print('|', ' | '.join([
                        self.pad_right(response['StackSetOperation']['CreationTimestamp']
                                       .strftime("%m/%d/%Y, %H:%M:%S"), 20),
                        self.pad_right(operation_id, 128),
                        self.pad_right(response['StackSetOperation']['Action'], 6),
                        self.colorize_operation_status(response['StackSetOperation']['Status']) + self.pad_right('', 15 - len(response['StackSetOperation']['Status'])),
                        self.pad_right(response['StackSetOperation']['ExecutionRoleName'], 99)
                    ]), '|')
                    heartbeat_timer = 0  # reset the timer
            if operation_current_status in self.END_STATES:
                operation_final_status = operation_current_status
                break
            # pause between requests to avoid hammering the API
            sleep(SLEEP_SECONDS)
            heartbeat_timer += SLEEP_SECONDS
        if send_heartbeat:
            print('-' * TABLE_CHARACTER_WIDTH)
        else:
            print(operation_id, 'finished. Proceeding...')
        return operation_final_status
