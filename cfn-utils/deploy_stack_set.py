#! /usr/bin/python3
"""Command line arguments and execution for creating/updating CloudFormation stack sets."""

import argparse
import deploy

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--stack-set-name',
                    '-s',
                    help="the name of the CloudFormation stack set",
                    type=str)
PARSER.add_argument('--description',
                    '-d',
                    default='No description given',
                    help="the description of the stack set",
                    type=str)
# PARSER.add_argument('--accounts',
#                     '-a',
#                     help='account IDs, comma or comma-space-separated, for stack instances',
#                     type=str)
PARSER.add_argument('--base-template',
                    '-t',
                    help="the main template path or URL",
                    type=str)
PARSER.add_argument('--capabilities',
                    '-c',
                    help="the capabilities for CloudFormation",
                    type=str)
PARSER.add_argument('--operation-preferences',
                    '-o',
                    default='MaxConcurrentPercentage=50,FailureTolerancePercentage=10',
                    help="operation preferences for stack set updates",
                    type=str)
# PARSER.add_argument('--regions',
#                     '-r',
#                     help='regions for stack instances',
#                     type=str)

ARGS = PARSER.parse_args()
STACK_SET_NAME = ARGS.stack_set_name
DESCRIPTION = ARGS.description
# ACCOUNTS = ARGS.accounts
BASE_TEMPLATE = ARGS.base_template
CAPABILITIES = ARGS.capabilities
OPERATION_PREFERENCES = ARGS.operation_preferences
# REGIONS = ARGS.regions

D = deploy.StackSet(STACK_SET_NAME)
D.deploy_stack_set(BASE_TEMPLATE,
                   # ACCOUNTS,
                   # REGIONS,
                   CAPABILITIES,
                   DESCRIPTION,
                   OPERATION_PREFERENCES)
