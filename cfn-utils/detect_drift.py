#! /usr/bin/python3
"""Command line arguments and execution for detecting CloudFormation stack drift."""

import argparse
import sys
import deploy

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--stack-name',
                    '-s',
                    help="the name of the CloudFormation stack",
                    type=str)

# print(parser.format_help())
# usage: test_args_4.py [-h] [--foo FOO] [--bar BAR]
# optional arguments:
#   -h, --help         show this help message and exit
#   --foo FOO, -f FOO  a random options
#   --bar BAR, -b BAR  a more random option

ARGS = PARSER.parse_args()
STACK_NAME = ARGS.stack_name

D = deploy.Stack(STACK_NAME)
STACK_STATUS = D.get_stack_status()
if STACK_STATUS in ["CREATE_COMPLETE",
                    "UPDATE_COMPLETE",
                    "UPDATE_ROLLBACK_COMPLETE"]:
    DRIFTS = D.check_for_drift()
    if not DRIFTS['StackResourceDrifts']:
        print('No drift was detected.')
        sys.exit()
    else:
        print('Drift was detected.')
        sys.exit(1)
elif STACK_STATUS in ['DELETE_COMPLETE', 'undefined']:
    D.no_drift_possible()
else:
    D.drift_detection_error()
